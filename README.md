# Script de configuration post-installation pour serveurs CentOS7

(c) Niki Kovacs, 2020

Ce dépot met à disposition un script "automagique" de configuration pour serveurs 
CentOS 7 après installation ainsi qu'un ensemble de scripts d'aide et des 
templates de fichiers de configuration pour les services communément utilisés.

## Pour résumer

Effectue les étapes suivantes.

  1. installer une version minimale de CentOS 7

  2. Créer un utilisateur non-'root' avec les droits d'administration.

  3. installer Git: 'sudo yum install git'

  4. Télécharger le script: 'git clone https://gitlab.com/kikinovak/centos-7.git'

  5. Se rendre dans le nouveau dossier: 'cd centos-7'

  6. Exécuter le script: 'sudo ./centos-setup.sh --setup'

  7. Déguster son caffé pendant que le script travaille à notre place.

  8. Redémarrer.

## Personnaliser un serveur CentOS

Faire un serveur fonctionnel depuis une installation minimale de CentOS se 
résume en une séries d'opérations plus ou moins fastidieuses. Votre procédure
peut évidemment varier, mais voici celle que j'applique habituellement après
une nouvelle installation de CentOS.


  * Personnalisation du shell Bash: Invite de commande, alias, etc.

  * Personnalisation de l'éditeur Vim.

  * Mise en place des dépots officels et tierces.

  * installation d'une suite d'outils de ligne de commande.

  * Suppression de multiple packages non-nécessaires.

  * Permettre à l'utilisateur admin d'accèder aux logs système.

  * Désactiver IPV6 et reconfigurer les services en fonction.
  
  * Définir un mot de passe permanent pour 'sudo'.

  * Etc.

Le script 'centos-setup.sh' effectue toutes ces opérations.


Configurer Bash et Vim et mettre en place une résolution par défaut de la console
plus lisible:

```
# ./centos-setup.sh --shell
```

Mise en place des dépots officiels et tierces:

```
# ./centos-setup.sh --repos
```

Installation des groupes de paquets `Core` et `Base` ainsi que quelques outils
supplémentaires:

```
# ./centos-setup.sh --extra
```

Suppression de multiples paquet non nécessaires:

```
# ./centos-setup.sh --prune
```

Permettre l'accès aux logs système à l'utilisateur admin:

```
# ./centos-setup.sh --logs
```

Désactiver IPV6 et reconfiguration des services de base en fonction:

```
# ./centos-setup.sh --ipv4
```

Définir un mot de passe persistant pour sudo:

```
# ./centos-setup.sh --sudo
```

Perform all of the above in one go:

```
# ./centos-setup.sh --setup
```

Retire les paquets et revient à un système de base renforcé:

```
# ./centos-setup.sh --strip
```

Affiche l'aide:

```
# ./centos-setup.sh --help
```

Si vous voulez connaitre le déroulement exact, ouvrir un second terminal et 
visionner les logs:

```
$ tail -f /tmp/centos-setup.log
```

